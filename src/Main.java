import burgers.*;

public class Main {
    public static void main(String[] args) {
        Hamburger chickenBurger = new Hamburger("Chicken Burger","white", "chicken", 2.6);
        chickenBurger.addTomato();
        chickenBurger.addCarrot();
        System.out.println(chickenBurger.getTotalSum());

        Hamburger cheeseBurger = new Hamburger("Cheese Burger","white", "pork", 2.8);
        cheeseBurger.addCheese();
        cheeseBurger.addTomato();
        System.out.println(cheeseBurger.getTotalSum());

        HealthyBurger healthyChicken = new HealthyBurger("Healthy Chicken Burger","chicken", 3.2);
        healthyChicken.addTomato();
        healthyChicken.addEgg();
        System.out.println(healthyChicken.getTotalSum());

        DeluxeBurger deluxeBurger = new DeluxeBurger("Deluxe Beef Burger", "white", "beef", 2.0);
        System.out.println(deluxeBurger.getTotalSum());
    }
}
