package burgers;

import ingredients.Cucumber;
import ingredients.Egg;

public class HealthyBurger extends Hamburger {
    private Cucumber cucumber;
    private Egg egg;

    public HealthyBurger(String name, String meat, double price) {
        super(name, "brown rye bread roll", meat, price);
    }

    public void addCucumber() {
        this.cucumber = new Cucumber();
        updateTotalPrice(cucumber.getPrice());
    }

    public void addEgg() {
        this.egg = new Egg();
        updateTotalPrice(egg.getPrice());
    }

    @Override
    protected String getPriceInfo() {
        String baseInfo = super.getPriceInfo();

        if (this.cucumber != null) {
            baseInfo += "Cucumber:\t\t\t" + cucumber.getStringPrice() + "$\n";
        }
        if (this.egg != null) {
            baseInfo += "Egg:\t\t\t" + egg.getStringPrice() + "$\n";
        }

        return baseInfo;
    }
}
