package burgers;

import ingredients.*;

public class DeluxeBurger extends Hamburger {
    private Chips chips;
    private Drink drink;

    private String errorMsg = "It is not possible add extra additional to Deluxe Burger.";

    public DeluxeBurger(String name, String breadRole, String meat, double price) {
        super(name, breadRole, meat, price);
        addChips();
        addDrink();
    }

    private void addChips() {
        this.chips = new Chips();
        updateTotalPrice(chips.getPrice());
    }

    private void addDrink() {
        this.drink = new Drink();
        updateTotalPrice(drink.getPrice());
    }

    @Override
    public void addLettuce() {
        System.out.println(errorMsg);
    }

    @Override
    public void addCarrot() {
        System.out.println(errorMsg);
    }

    @Override
    public void addCheese() {
        System.out.println(errorMsg);
    }

    @Override
    public void addTomato() {
        System.out.println(errorMsg);
    }

    @Override
    protected String getPriceInfo() {
        String baseInfo = super.getPriceInfo();

        if (this.chips != null) {
            baseInfo += "Chips:\t\t\t" + chips.getStringPrice() + "$\n";
        }
        if (this.drink != null) {
            baseInfo += "Drink:\t\t\t" + drink.getStringPrice() + "$\n";
        }

        return baseInfo;
    }
}
