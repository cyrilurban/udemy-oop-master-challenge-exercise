package burgers;

import ingredients.Carrot;
import ingredients.Cheese;
import ingredients.Lettuce;
import ingredients.Tomato;


public class Hamburger {
    private String name;

    private String breadRole;
    private String meat;
    private double price;

    private double totalPrice;

    private Lettuce lettuce;
    private Carrot carrot;
    private Cheese cheese;
    private Tomato tomato;

    public Hamburger(String name, String breadRole, String meat, double price) {
        this.name = name;
        this.breadRole = breadRole;
        this.meat = meat;
        this.price = price;

        this.totalPrice = price;
    }

    public void addLettuce() {
        this.lettuce = new Lettuce();
        updateTotalPrice(lettuce.getPrice());
    }

    public void addCarrot() {
        this.carrot = new Carrot();
        updateTotalPrice(carrot.getPrice());
    }

    public void addCheese() {
        this.cheese = new Cheese();
        updateTotalPrice(cheese.getPrice());
    }

    public void addTomato() {
        this.tomato = new Tomato();
        updateTotalPrice(tomato.getPrice());
    }

    protected void updateTotalPrice(double newPrice) {
        this.totalPrice += newPrice;
    }

    protected String getPriceInfo() {
        String summary = this.name.toUpperCase() + "\n";
        summary += "Base Hamburger:\t" + String.format("%.2f", this.price) + "$\n";

        if (this.lettuce != null) {
            summary += "Lettuce:\t\t\t" + lettuce.getStringPrice() + "$\n";
        }
        if (this.carrot != null) {
            summary += "Carrot:\t\t\t" + carrot.getStringPrice() + "$\n";
        }
        if (this.cheese != null) {
            summary += "Cheese:\t\t\t" + cheese.getStringPrice() + "$\n";
        }
        if (this.tomato != null) {
            summary += "Tomato:\t\t\t" + tomato.getStringPrice() + "$\n";
        }

        return summary;
    }

    public String getTotalSum() {
        String sum = getPriceInfo();
        sum += "---------------------\n";
        sum += "Summary:\t\t" + String.format("%.2f", this.totalPrice) + "$\n";

        return sum;
    }
}
