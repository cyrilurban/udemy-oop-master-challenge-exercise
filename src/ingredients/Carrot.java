package ingredients;

import ingredients.base.Ingredient;

public class Carrot extends Ingredient {
    public Carrot() {
        super(0.4);
    }
}