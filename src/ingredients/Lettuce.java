package ingredients;

import ingredients.base.Ingredient;

public class Lettuce extends Ingredient {
    public Lettuce() {
        super(0.8);
    }
}
