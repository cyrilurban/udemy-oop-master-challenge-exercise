package ingredients;

import ingredients.base.Ingredient;

public class Cucumber extends Ingredient {
    public Cucumber() {
        super(0.4);
    }
}
