package ingredients;

import ingredients.base.Ingredient;

public class Egg extends Ingredient {
    public Egg() {
        super(0.9);
    }
}
