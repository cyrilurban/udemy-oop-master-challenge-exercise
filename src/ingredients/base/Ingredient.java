package ingredients.base;

public class Ingredient {
    private double price;

    public Ingredient(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public String getStringPrice() {
        return String.format("%.2f", price);
    }
}
