package ingredients;

import ingredients.base.Ingredient;

public class Tomato extends Ingredient {
    public Tomato() {
        super(0.3);
    }
}