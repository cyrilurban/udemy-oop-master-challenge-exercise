package ingredients;

import ingredients.base.Ingredient;

public class Chips extends Ingredient {
    public Chips() {
        super(1.5);
    }
}
