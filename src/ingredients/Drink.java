package ingredients;

import ingredients.base.Ingredient;

public class Drink extends Ingredient {
    public Drink() {
        super(1.0);
    }
}
