package ingredients;

import ingredients.base.Ingredient;

public class Cheese extends Ingredient {
    public Cheese() {
        super(0.8);
    }
}